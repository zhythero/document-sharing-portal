<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$dummyUsers = [];
    	for ($i = 0; $i < 50; $i++) {
    		$dummyUsers[] = [
	            'name' => str_random(8),
	            'email' => str_random(12).'@mail.com',
	            'password' => Hash::make('password'),
	            'role' => 'user',
	            'created_at' => '2016-09-02 01:02:03',
	            'updated_at' => '2016-09-02 01:02:03'
	        ];
    	}

        DB::table('users')->insert($dummyUsers);
    }
}
