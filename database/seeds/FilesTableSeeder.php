<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$dummyFiles = [];
    	for ($i = 0; $i < 50; $i++) {
    		$dummyFiles[] = [
	            'filename' => str_random(8) . '.docx',
	            'uploader' => 2
	        ];
    	}

        DB::table('files')->insert($dummyFiles);
    }
}
