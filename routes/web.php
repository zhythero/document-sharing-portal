<?php

Route::group(['middleware' => ['auth','web']], function() {
	
	/*
	|--------------------------------------------------------------------------
	| Admin Routes
	|--------------------------------------------------------------------------
	*/
	Route::group(['prefix' => 'admin', 'middleware' => ['admin'] ], function() {
		Route::get('/', 'AdminController@showUsersManagementPage');
		Route::get('/users', 'AdminController@showUsersManagementPage');
		Route::post('/users', 'AdminController@createUser');
		Route::post('/users/{user_id}', 'AdminController@deleteUser');
	});

	/*
	|--------------------------------------------------------------------------
	| User Routes
	|--------------------------------------------------------------------------
	*/
	Route::get('/', 'HomeController@index');
	Route::get('/home', 'HomeController@index');
	Route::post('/files', 'FilesController@upload');
	Route::post('/files/share', 'HomeController@shareFile');
});

Auth::routes();
