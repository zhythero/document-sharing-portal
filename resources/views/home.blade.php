@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="row">

        <!-- Files -->
        <div class="col-md-8">
            <h4>My Files</h4>
            <p class="text-muted">My files and files shared to me.</p>
            @if ($files->isEmpty())
                <p>You have no files :( <br> Upload files now.</p>
            @endif
            <ul class="list-group">
                @foreach ($files as $file)
                    <li class="list-group-item">
                        <strong>{{ $file->filename }}</strong> by {{ $file->uploader->name }}<p class="pull-right text-muted">{{ $file->created_at }}</p>
                        <p>Owners: @foreach ($file->owners as $owner) <span><strong>{{ $owner->name }}</strong></span>, @endforeach</p>
                        <form method="POST" action="{{ url('/files/share') }}">
                            <input type="hidden" name="file_id" value="{{ $file->id }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>Share with</label>
                                <input type="" class="form-control input-sm" name="email" placeholder="Type user's email here">
                            </div>
                            <button class="btn btn-primary btn-sm" type="submit">Share</button>
                        </form>
                    </li>
                @endforeach
            </ul>
            <div class="text-center">
                {{ $files->links() }}
            </div>
        </div>

        <!-- New File -->
        <div class="col-md-4">

            <!-- New File Form -->
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Upload file
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ url('/files') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>File</label>
                            <input type="file" class="form-control input-sm" name="file">
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm">Upload</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
