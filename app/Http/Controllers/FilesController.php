<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use App\File;
use App\FileOwner;

use Auth;

class FilesController extends Controller
{
    public function upload(Request $request) {

    	$this->validate($request, [
    		'file' => 'file|mimes:pdf,doc,xls,xlsx,docx,png,jpg,bmp,gif,jpeg|unique:files,filename'
		], [
			'file.unique' => 'The file already exists. Try renaming the file.'
		]);

    	$request->file('file')
    		->storeAs('uploaded_files', $request->file('file')->getClientOriginalName(), 'local');

    	$createdFile = File::create([
    		'filename' => $request->file('file')->getClientOriginalName(),
    		'uploader_id' => Auth::id()
		]);

		FileOwner::create([
			'file_id' => $createdFile->id,
			'user_id' => Auth::id()
		]);

		return redirect()->back();
    }
}
