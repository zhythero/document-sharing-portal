<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\File;
use App\FileOwner;
use App\User;

use Auth;

class HomeController extends Controller
{
    public function index()
    {
    	$files = File::orderBy('id','DESC')
    		->with('uploader')
    		->with('owners')
    		->whereHas('owners', function($query) {
    			$query->where('user_id', Auth::id());
    		})
    		->paginate(15);



    	return view('home')
    		->with([
    			'files'=>$files
			]);
    }

    public function shareFile(Request $request) {

    	$this->validate($request, [
    		'email' => 'required|exists:users,email'
    	], [
			'email.exists' => 'User email not found.'
		]);

    	FileOwner::create([
    		'user_id' => User::where('email',$request->input('email'))->get()->first()->id,
    		'file_id' => $request->input('file_id')
		]);

		return redirect()->back();
    }

}
