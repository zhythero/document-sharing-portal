<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class AdminController extends Controller
{
    public function showUsersManagementPage() {

    	$users = User::orderBy('id', 'DESC')->where('role', 'user')->paginate(15);

    	return view('admin.users')->with('users', $users);
    }

    public function createUser(Request $request) {

    	$this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed'
        ]);

        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        return redirect()->back();

    }

    public function deleteUser($userId) {
    	
    	User::where('id', $userId)->delete();

    	return redirect()->back();
    }
}
