<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['filename','uploader_id'];

    public function owners() {
        return $this->belongsToMany('App\User', 'file_owners');
    }

    public function uploader() {
        return $this->belongsTo('App\User', 'uploader_id');
    }

}
